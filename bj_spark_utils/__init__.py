"""Top-level package for bj-spark-utils."""

__author__ = """Sudipta Basak"""
__email__ = 'sudipta.basak-v@barrenjoey.com'
__version__ = '0.1.0'
