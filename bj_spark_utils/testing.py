from pyspark.sql import functions as F


def assert_same(actual, expected):
    """
    Compares two dataframes contents and throws an assertion error if the schema are different or the contents
    are not the same
    """
    actual_columns = set(actual.columns)
    expected_columns = set(expected.columns)
    columns_missing_from_actual = expected_columns - actual_columns
    columns_missing_from_expected = actual_columns - expected_columns

    assert len(columns_missing_from_actual) == 0 and len(columns_missing_from_expected) == 0, \
        failed_assertion_column_message(list(actual_columns), list(expected_columns),
                                        list(columns_missing_from_actual), list(columns_missing_from_expected))

    actual = actual.select(expected.columns)
    rows_missing_from_actual = expected.subtract(actual).collect()
    rows_missing_from_expected = actual.subtract(expected).collect()

    assert len(rows_missing_from_actual) == 0 and len(rows_missing_from_expected) == 0, \
        failed_assertion_rows_message(actual.collect(), expected.collect(), rows_missing_from_actual,
                                      rows_missing_from_expected)

    actual_count = actual.count()
    expected_count = expected.count()
    assert actual_count == expected_count, \
        "Got an unexpected result:\n" + \
        "Expected " + str(expected_count) + " but got " + str(actual_count)


def failed_assertion_rows_message(actual_rows, expected_rows, rows_missing_from_actual, rows_missing_from_expected):
    return "Got an unexpected result:\n" + rows_to_string(actual_rows) + "\n\nExpected:\n" + rows_to_string(
        expected_rows) \
           + "\n\nGot the following rows in expected but not in actual:\n" + rows_to_string(rows_missing_from_actual) \
           + "\n\nGot the following rows in actual but not in expected:\n" + rows_to_string(rows_missing_from_expected)


def failed_assertion_column_message(actual_columns, expected_columns, cols_missing_from_actual,
                                    cols_missing_from_expected):
    return "Got an unexpected result:\n" + actual_columns + "\n\nExpected:\n" + ", ".join(expected_columns) \
           + "\n\nGot the following columns in expected but not in actual:\n" + ", ".join(cols_missing_from_actual) \
           + "\n\Got the following columns in actual but not in expected:\n" + ", ".join(cols_missing_from_expected)


def rows_to_string(rows):
    return "\n".join([", ".join(str(v) for v in row) for row in rows])


def assert_column_in_df(df, column_name):
    assert column_name in df.columns, "{column} should exist in df, but received {columns}".format(column=column_name,
                                                                                                   columns=str(
                                                                                                       df.columns))


def assert_column_not_in_df(df, column_name):
    assert column_name not in df.columns, "{column} should not exist in df, but received {columns}".format(
        column=column_name, columns=str(df.columns))


def assert_string_column_max_length(df, column_name, max_length):
    """
    Assert that NO string values in df[column_name] are longer than max_length characters.
    """
    offending_df = df.filter(F.length(F.col(column_name)) > max_length)
    assert offending_df.rdd.isEmpty()


def assert_hubble_primary_key_length(df, column_name):
    """
    Assert that df[column_name] is conforming AtlasDB limitation of maximum 1500 characters for primary keys.
    """
    ATLASDB_MAX_PRIMARY_KEY_LENGTH = 1500
    assert_string_column_max_length(df, column_name, ATLASDB_MAX_PRIMARY_KEY_LENGTH)
