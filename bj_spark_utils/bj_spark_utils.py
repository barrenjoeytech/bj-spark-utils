"""Main module."""
from pyspark.sql import DataFrame
from pyspark.sql import functions as F
from pyspark.sql import types as T
from pyspark.sql import Window
import inspect
import datetime
from pyspark.storagelevel import StorageLevel
from functools import wraps
from functools import reduce


def is_primary_key(df, primary_key_columns):
    """
    Return True if the columns in 'primary_key_columns' are unique across all rows
    """
    duplicate_count = df \
        .groupBy(primary_key_columns) \
        .count() \
        .filter(F.col("count") > 1) \
        .limit(1) \
        .count()

    return duplicate_count == 0


def to_utc_date(timestamp_col):
    """
        converts the given timestamp column to utc date. This function converts to UTC date regardless of the server's/jvm's timezone.
    """
    if isinstance(timestamp_col, str):
        timestamp_col = F.col(timestamp_col)

    return (F.floor(timestamp_col.cast("long") / (24 * 60 * 60)) * (24 * 60 * 60)).cast("timestamp").cast("date")


def union_different_columns(*dfs):
    all_columns = set.union(*[set(df.columns) for df in dfs])
    column_types = dict(set((k, v) for df in dfs for k, v in df.dtypes))
    dataframes_with_common_columns = []
    for i, df in enumerate(dfs):
        missing_columns = all_columns - set(df.columns)

        df = df.select(
            *([F.lit(None).cast(column_types[col]).alias(col) for col in missing_columns] +
              [F.col(col) for col in df.columns]))

        dataframes_with_common_columns.append(df)
    return reduce(DataFrame.unionByName, dataframes_with_common_columns)


def max_row(df, comparison_key, *groupby_keys):
    '''
        Max row takes a key or set of groupby_keys, groups by those groupby_keys and returns a single row from each
        group that contains the max value of some comparison column
    '''
    ordering = F.col(comparison_key).desc()
    w = Window().partitionBy(*groupby_keys).orderBy(ordering)
    top = df.withColumn("rn", F.row_number().over(w)).where(F.col('rn') == 1).drop(F.col('rn'))
    return top


def min_row(df, comparison_key, *groupby_keys):
    '''
        Min row takes a key or set of groupby_keys, groups by those groupby_keys and returns a single row from each
        group that contains the min value of some comparison column
    '''
    ordering = F.col(comparison_key).asc()
    w = Window().partitionBy(*groupby_keys).orderBy(ordering)
    top = df.withColumn("rn", F.row_number().over(w)).where(F.col('rn') == 1).drop(F.col('rn'))
    return top


def deskewed_join(large_df, small_df, join_keys, join_type, n=16):
    """
    Adds randomness to join keys to avoid issues that arise from skewed joins. It assumes that one side
    of the join is large and the other is small, and you must correctly specify these in the arguments.

    Note: this currently only supports a large DF on the left and a small DF on the right.

    Input:
    - large_df: The large dataframe in the join
    - small_df: The small dataframe in the join
    - join_keys: A list of column names that form the join key
    - join_type: One of PySpark's allowed join types, e.g. "left", "inner"
    - n (optional): the number of buckets to randomly split the dataframes into

    Output:
    - The result of the join as you would normally expect
    """
    large_df = large_df \
        .withColumn("replication_id", F.floor((F.rand() * (n - 1))))

    small_df = small_df \
        .withColumn("replication_id", F.array([F.lit(i) for i in range(n)])) \
        .select(F.explode("replication_id").alias("replication_id"), *small_df.columns)

    new_key = ["replication_id"] + join_keys
    result = large_df.join(small_df, new_key, join_type)
    return result.drop("replication_id")


def add_to_date_using_from_date(df, _from_date_col_name, from_date_col_name, to_date_col_name, *groupby_keys):
    null_date = datetime.datetime.strptime("3000-01-01", "%Y-%m-%d").date()

    # Date columns need to be of Date type
    ordering = F.col(_from_date_col_name).desc()
    window = Window().partitionBy(*groupby_keys).orderBy(ordering)
    return df \
        .withColumnRenamed(
        _from_date_col_name,
        from_date_col_name
    ).withColumn(
        to_date_col_name,
        F.lag(from_date_col_name, 1, None).over(window)
    ).withColumn(
        to_date_col_name,
        F.when(F.col(to_date_col_name).isNull(), null_date).otherwise(F.col(to_date_col_name))
    ).withColumn(
        to_date_col_name,
        F.date_sub(F.col(to_date_col_name), 1)
    )


def add_from_date_using_date_column(df, _date_col_name, from_date_col_name, *groupby_keys):
    w = Window().partitionBy(*groupby_keys)

    return df \
        .withColumn(
        "_min",
        F.to_date(F.lit("1970-01-01"), "yyyy-MM-dd")
    ).withColumn(
        "_min_date",
        F.min(_date_col_name).over(w)
    ).withColumn(
        from_date_col_name,
        F.when(
            F.col(_date_col_name) == F.col("_min_date"), F.col("_min")
        ).otherwise(F.col(_date_col_name))
    ).drop(
        "_min", "_min_date"
    )


def add_valid_to_using_valid_from(df, _valid_from_column, groupby_keys):
    null_date = F.lit(32503593600).cast(T.TimestampType())

    # Date columns need to be of Date type
    ordering = F.col(_valid_from_column).desc()
    window = Window().partitionBy(*groupby_keys).orderBy(ordering)
    return df \
        .withColumnRenamed(
        _valid_from_column,
        "_valid_from"
    ).withColumn(
        "_valid_to",
        F.lag("_valid_from", 1, None).over(window)
    ).withColumn(
        "_valid_to",
        F.when(F.col("_valid_to").isNull(), null_date).otherwise(F.col("_valid_to")).cast(T.TimestampType())
    )


def cast_to_schema(schema):
    """
    This should be used as a decorator around a transform to cast the output of the transform into the
    specified schema. It does that by:
        1. Adding (nullable) missing columns to the output dataset (if it can't it errors)
        2. Dropping any extra columns in the output dataset
        3. Casting columns in the dataset to the type specified by the schema  (if it can't it errors)
    """

    def do_casting(df):
        for field in schema.fields:
            if field.name not in df.columns:
                assert field.nullable == True, \
                    "output doesn't contain not nullable field %r" % field.name
                df = df.withColumn(field.name, F.lit(None).cast(field.dataType))
        return df.select(*[F.col(field.name).cast(field.dataType) for field in schema.fields])

    def wrap(wrapped_f):
        if 'ctx' in inspect.getargspec(wrapped_f)[0]:
            def wrapper(ctx, **kwargs):
                return do_casting(wrapped_f(ctx, **kwargs))
        else:
            def wrapper(**kwargs):
                return do_casting(wrapped_f(**kwargs))

        wrapper.__name__ = wrapped_f.__name__
        return wrapper

    return wrap


def assert_unique(col_names):
    """
    This should be used as a decorator around a transform to fail the transform if the combination of col_names in the output doesn't make
    up a unique combination.
    """

    def do_assert_unique(df):
        df.persist(StorageLevel.DISK_ONLY)
        assert is_primary_key(df, col_names), \
            ", ".join(col_names) + " is not unique (there could be nulls in the columns)"
        return df

    def wrap(wrapped_f):
        if 'ctx' in inspect.getargspec(wrapped_f)[0]:
            def wrapper(ctx, **kwargs):
                return do_assert_unique(wrapped_f(ctx, **kwargs))
        else:
            def wrapper(**kwargs):
                return do_assert_unique(wrapped_f(**kwargs))

        wrapper.__name__ = wrapped_f.__name__
        return wrapper

    return wrap


def enforce_output_schema(expected_schema, enforce_same_order=False, ignore_nullable=False):
    """
    This should be used as a decorator around a transform to check the output meets a given schema.
    This decorator doesn't do any casting or dropping/creating of columns. It will throw an error
    if there's any discrepency between the expected output schema and the actual schema.
    """

    def compare_schemas_ignoring_nullable_and_order(actual, expected):
        actual_converted = set(convert_to_field_name_tuples(actual))
        expected_converted = set(convert_to_field_name_tuples(expected))
        if actual_converted != expected_converted:
            reason = "\n".join([
                "Output schema does not match expected schema (ignoring nullable and order).",
                "Expected schema: {}".format(expected),
                "Actual schema: {}".format(actual)
            ])
            raise ValueError(reason)

    def compare_schemas_ignoring_nullable(actual, expected):
        actual_converted = convert_to_field_name_tuples(actual)
        expected_converted = convert_to_field_name_tuples(expected)
        if actual_converted != expected_converted:
            reason = "\n".join([
                "Output schema does not match expected schema (ignoring nullable).",
                "Expected schema: {}".format(expected),
                "Actual schema: {}".format(actual)
            ])
            raise ValueError(reason)

    def compare_schemas_ignoring_order(actual, expected):
        actual_column_set = set(actual)
        expected_column_set = set(expected)
        if actual_column_set != expected_column_set:
            reason = "\n".join([
                "Output schema does not match expected schema, even when ignoring column order.",
                "Expected columns missing in output schema: {}".format(expected_column_set - actual_column_set),
                "Unexpected columns in output schema: {}".format(actual_column_set - expected_column_set)
            ])
            raise ValueError(reason)

    def compare_schemas(actual, expected, enforce_same_order=False, ignore_nullable=False):
        if actual != expected:
            reason = "\n".join([
                "Output schema does not match expected schema.",
                "Expected schema: {}".format(expected),
                "Actual schema: {}".format(actual)
            ])
            raise ValueError(reason)

    def convert_to_field_name_tuples(schema):
        tups = []
        for field in schema.fields:
            dataType = field.dataType
            if hasattr(dataType, "fields"):
                dataType = str(convert_to_field_name_tuples(dataType))
            if hasattr(dataType, "elementType"):
                if hasattr(dataType.elementType, "fields"):
                    dataType = str([convert_to_field_name_tuples(dataType.elementType)])
                else:
                    dataType = str([dataType.elementType])
            tups.append((field.name, dataType))
        return tups

    def do_check_output_schema(output_dataframe):
        if enforce_same_order and ignore_nullable:
            compare_schemas_ignoring_nullable(output_dataframe.schema, expected_schema)
        elif enforce_same_order:
            compare_schemas(output_dataframe.schema, expected_schema)
        elif ignore_nullable:
            compare_schemas_ignoring_nullable_and_order(output_dataframe.schema, expected_schema)
        else:
            compare_schemas_ignoring_order(output_dataframe.schema, expected_schema)
        return output_dataframe

    def check_output_schema(wrapped_f):
        if 'ctx' in inspect.getargspec(wrapped_f)[0]:
            def wrapper(ctx, **kwargs):
                return do_check_output_schema(wrapped_f(ctx, **kwargs))
        else:
            def wrapper(**kwargs):
                return do_check_output_schema(wrapped_f(**kwargs))

        wrapper.__name__ = wrapped_f.__name__
        return wrapper

    return check_output_schema


def memoize(function):
    memo = {}

    @wraps(function)
    def wrapper(*args):
        try:
            return memo[args]
        except KeyError:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


def convert_string_or_column_to_column(col):
    if isinstance(col, str):
        return F.col(col)
    else:
        return col
