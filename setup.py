#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [ ]

test_requirements = ['pytest>=3', ]

setup(
    author="Sudipta Basak",
    author_email='sudipta.basak-v@barrenjoey.com',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Spark Utils",
    install_requires=requirements,
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='bj_spark_utils',
    name='bj_spark_utils',
    packages=find_packages(include=['bj_spark_utils', 'bj_spark_utils.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/basaks1/bj_spark_utils',
    version='0.1.0',
    zip_safe=False,
)
